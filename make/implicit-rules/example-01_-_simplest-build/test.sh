#!/bin/bash

# make binary to call
make='make --no-print-directory'

# make arguments
mkargs=""

echo "--------------------------------------------------------------------------"
echo "Test 1: Clean"
mkargs='-C project/ clean'
echo "'\$ $make $mkargs'"
echo ""
$make $mkargs
echo "--------------------------------------------------------------------------"
echo "Test 2: Build"
mkargs='-C project/'
echo "'\$ $make $mkargs'"
echo ""
$make $mkargs
echo "--------------------------------------------------------------------------"
