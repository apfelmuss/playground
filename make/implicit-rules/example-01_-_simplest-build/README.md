# Introduction

This example shows how to build a C++-program using implicit rules. This may
not be a proper solution if you have a complex build using different libraries,
source folder, and so on. But for little test-/helper-programs this might be
very useful.

# TL;DR

If you have a single source file `main.cc` or `main.cpp` use a makefile with
following content:
```
.PHONY: clean
main:
clean: ; $(RM) main
```

# Description

If you have a single C++ source file, `main.cc` or `main.cpp`, the probably
simplest makefile could be:
```
.PHONY: clean
main:
clean: ; $(RM) main
```

1. An ordinary rule with no recipe (`main:`) triggers the implicit rule search
   algorithm for each target of the rule. (`main: ;` it's an ordinary rule with
   an empty recipe for the target `main`, which doesn't trigger the implicit
   rule search algorithm.)

2. Find a pattern rule using the implicit search algorithm.

   There are already builtin pattern rules like `%: %.cc` and `%: %.cpp` with a
   recipe how to make the target `%`. If the prerequisite for that pattern
   rules exist (`%.cc` or `%.cpp`), then this rule applies. This means in our
   case, if the file `main.cc` or `main.cpp` exist the rule will be used.

   Builtin implicit pattern rules:
   ```
   %: %.cc
       $(LINK.cc) $^ $(LOADLIBES) $(LDLIBS) -o $@

   %: %.cpp
       $(LINK.cpp) $^ $(LOADLIBES) $(LDLIBS) -o $@
   ```

3. The predefined variables `LINK.cc` and `LINK.cpp` exapnding in
   `$(CXX) $(CXXFLAGS) $(CPPFLAGS) $(LDFLAGS) $(TARGET_ARCH)`

4. Therefore, our explicit rule without recipe (`main:`) will be replaced with:
   ```
   main: main.cc
       $(CXX) $(CXXFLAGS) $(CPPFLAGS) $(LDFLAGS) $(TARGET_ARCH) $^ $(LOADLIBES) $(LDLIBS) -o $@
   ```

5. Builtin implicit variables used by builtin implicit pattern rules:

   - `CXX`: Program for compiling C++ programs. default: ‘g++’

   - `CXXFLAGS`: Extra flags to give to the C++ compiler: default: ’’

   - `CPPFLAGS`: Extra flags to give to the C preprocessor and programs that
     use it (the C and Fortran compilers). default: ’’

   - `LDFLAGS`: Extra flags to give to compilers when they are supposed to
     invoke the linker, ‘ld’, such as -L. Libraries (-lfoo) should be added to
     the LDLIBS variable instead. default: ’’.

   - `TARGET_ARCH`: Not documented in GNU make manual. But most internet search
     hits say this is used for architecture-specific compilation options for
     cross-compilation. For example:<br />
     `TARGET_ARCH="-march=armv7ve+vfpv4+neon -mtune=cortex-a17"`

   - `LOADLIBES`: Deprecated, see LDLIBS.

   - `LDLIBS`: Library flags or names given to compilers when they are
     supposed to invoke the linker, ‘ld’. LOADLIBES is a deprecated (but still
     supported) alternative to LDLIBS. Non-library linker flags, such as -L,
     should go in the LDFLAGS variable. default: ’’

   - `^`: The names of all the prerequisites, with spaces between them. For
     prerequisites which are archive members, only the named member is used
     (see Archives). A target has only one prerequisite on each other file it
     depends on, no matter how many times each file is listed as a
     prerequisite. So if you list a prerequisite more than once for a target,
     the value of $^ contains just one copy of the name. This list does not
     contain any of the order-only prerequisites; for those see the ‘$|’
     variable.

   - `@`: The file name of the target of the rule. If the target is an archive
     member, then ‘$@’ is the name of the archive file. In a pattern rule that
     has multiple targets, ‘$@’ is the name of whichever target caused the
     rule’s recipe to be run.

Building a C++ program has two steps: compiling and linking. Therefore, `make`
has a builtin implicit rule for linking (take the object files and link them to
an executable binary file). The implicit search algorithm will find also
following builtin pattern rule for our example.
```
%: %.o
    $(LINK.o) $^ $(LOADLIBES) $(LDLIBS) -o $@
```
Then the algorithm sees that the prerequisite `%.o` ought to exist. This
prerequisite is not in the target of any rule and it comes from an implicit
rule. The search algorithm is triggered recursively to find a rule for that
prerequisite. This is called a chain of rules. Following builtin pattern rules
will be found to create the object file from compiling a source file:
```
%.o: %.cc
  $(COMPILE.cc) $(OUTPUT_OPTION) $<

%.o: %.cpp
  $(COMPILE.cpp) $(OUTPUT_OPTION) $<

```
See the defintion of the following variables:
```
LINK.o = $(CC) $(LDFLAGS) $(TARGET_ARCH)
COMPILE.cpp = $(COMPILE.cc)
COMPILE.cc = $(CXX) $(CXXFLAGS) $(CPPFLAGS) $(TARGET_ARCH) -c
OUTPUT_OPTION = -o $@
```
So, our rule `main:` could also be replaced by the matched rules:
```
main: main.o
    $(CC) $(LDFLAGS) $(TARGET_ARCH) $^ $(LOADLIBES) $(LDLIBS) -o main

main.o: main.cc
    $(CXX) $(CXXFLAGS) $(CPPFLAGS) $(TARGET_ARCH) -c -o main.o main.cc
```
Nowadays the two steps compiling and linking of a C++ program is done by
invoking the compiler only. The compiler then calls the linker. Therefore, the
implicit rule `%: %.cc ; $(LINK.cc) $^ $(LOADLIBES) $(LDLIBS) -o $@` from above
exist and this match has a higher priority, then the implicit rules for the
linking and compiling steps.

So, it's possible that more than one implicit rule can match. This is important
if you define your own implicit pattern rules and/or try to override the
builtin rules. To see which implicit rule applies you have to try to understand
the [implicit rule search algorithm](https://www.gnu.org/software/make/manual/html_node/Implicit-Rule-Search.html#Implicit-Rule-Search),
or call `make` with `-p` option and see which rules apply:
```
$ `make -p`
```
Extract from stdout:
```
# Variables

# default
LINK.o = $(CC) $(LDFLAGS) $(TARGET_ARCH)

# default
COMPILE.cpp = $(COMPILE.cc)

# default
LINK.cc = $(CXX) $(CXXFLAGS) $(CPPFLAGS) $(LDFLAGS) $(TARGET_ARCH)

# makefile
.DEFAULT_GOAL := main

# default
LINK.cpp = $(LINK.cc)

# default
OUTPUT_OPTION = -o $@

# default
COMPILE.cc = $(CXX) $(CXXFLAGS) $(CPPFLAGS) $(TARGET_ARCH) -c

# variable set hash-table stats:
# Load=172/1024=17%, Rehash=0, Collisions=18/214=8%

# Implicit Rules

%.o:

%: %.o
#  recipe to execute (built-in):
  $(LINK.o) $^ $(LOADLIBES) $(LDLIBS) -o $@

%.cc:

%: %.cc
#  recipe to execute (built-in):
  $(LINK.cc) $^ $(LOADLIBES) $(LDLIBS) -o $@

%.o: %.cc
#  recipe to execute (built-in):
  $(COMPILE.cc) $(OUTPUT_OPTION) $<

%.cpp:

%: %.cpp
#  recipe to execute (built-in):
  $(LINK.cpp) $^ $(LOADLIBES) $(LDLIBS) -o $@

%.o: %.cpp
#  recipe to execute (built-in):
  $(COMPILE.cpp) $(OUTPUT_OPTION) $<

# 92 implicit rules, 5 (5,4%) terminal.
# Files

# Not a target:
Makefile:
#  Implicit rule search has been done.
#  Last modified 2021-09-15 08:40:16.504640101
#  File has been updated.
#  Successfully updated.

clean:
#  Phony target (prerequisite of .PHONY).
#  Implicit rule search has not been done.
#  File does not exist.
#  File has not been updated.
#  recipe to execute (from 'Makefile', line 11):
  $(RM) main

# Not a target:
.cpp.o:
#  Builtin rule
#  Implicit rule search has not been done.
#  Modification time never checked.
#  File has not been updated.
#  recipe to execute (built-in):
  $(COMPILE.cpp) $(OUTPUT_OPTION) $<

# Not a target:
.cc:
#  Builtin rule
#  Implicit rule search has not been done.
#  Modification time never checked.
#  File has not been updated.
#  recipe to execute (built-in):
  $(LINK.cc) $^ $(LOADLIBES) $(LDLIBS) -o $@

# Not a target:
.DEFAULT:
#  Implicit rule search has not been done.
#  Modification time never checked.
#  File has not been updated.

main: main.cc
#  Implicit rule search has been done.
#  Implicit/static pattern stem: 'main'
#  Last modified 2021-09-16 09:32:03.854751635
#  File has been updated.
#  Successfully updated.
# automatic
# @ := main
# automatic
# * := main
# automatic
# < := main.cc
# automatic
# + := main.cc
# automatic
# % :=
# automatic
# ^ := main.cc
# automatic
# ? := main.cc
# automatic
# | :=
# variable set hash-table stats:
# Load=8/32=25%, Rehash=0, Collisions=1/21=5%
#  recipe to execute (built-in):
  $(LINK.cc) $^ $(LOADLIBES) $(LDLIBS) -o $@

# Not a target:
.o:
#  Builtin rule
#  Implicit rule search has not been done.
#  Modification time never checked.
#  File has not been updated.
#  recipe to execute (built-in):
  $(LINK.o) $^ $(LOADLIBES) $(LDLIBS) -o $@

# Not a target:
main.cc:
#  Implicit rule search has been done.
#  Last modified 2021-09-10 09:42:16.991672026
#  File has been updated.
#  Successfully updated.

# Not a target:
.cc.o:
#  Builtin rule
#  Implicit rule search has not been done.
#  Modification time never checked.
#  File has not been updated.
#  recipe to execute (built-in):
  $(COMPILE.cc) $(OUTPUT_OPTION) $<

# files hash-table stats:
# Load=77/1024=8%, Rehash=0, Collisions=121/1493=8%
```
In the output of `make -p` you can search for "Implicit rule search has been
done". You will find `main: main.cc`. You get the pattern stem "main" and
therefore the origin pattern rule is `%: %.cc`. If you search for it you will
find the used recipe.

# References

- [GNU make - Implicit Rules](https://www.gnu.org/software/make/manual/html_node/Implicit-Rules.html#Implicit-Rules)
- [GNU make - Using Implicit Rules](https://www.gnu.org/software/make/manual/html_node/Using-Implicit.html#Using-Implicit)
- [GNU make - Implicit Rule Search Algorithm](https://www.gnu.org/software/make/manual/html_node/Implicit-Rule-Search.html#Implicit-Rule-Search)
- [GNU make - Implicit Variables](https://www.gnu.org/software/make/manual/html_node/Implicit-Variables.html#Implicit-Variables)
- [GNU make - Automatic Variables](https://www.gnu.org/software/make/manual/html_node/Automatic-Variables.html#Automatic-Variables)
- [O'Reilly - Variables](https://www.oreilly.com/library/view/managing-projects-with/0596006101/ch03.html)
