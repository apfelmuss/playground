#!/bin/bash

# make binary to call
make='make --no-print-directory'

# make arguments
mkargs=""

echo "--------------------------------------------------------------------------"
echo "Test 1: Project 1 - Clean"
mkargs='-C project1/ clean'
echo "'\$ $make $mkargs'"
echo ""
$make $mkargs
echo "--------------------------------------------------------------------------"
echo "Test 2: Project 1 - Build"
mkargs='-C project1'
echo "'\$ $make $mkargs'"
echo ""
$make $mkargs
echo "--------------------------------------------------------------------------"
echo "Test 3: Project 2 - Clean"
mkargs='-C project2/ clean'
echo "'\$ $make $mkargs'"
echo ""
$make $mkargs
echo "--------------------------------------------------------------------------"
echo "Test 4: Project 2 - Build"
mkargs='-C project2'
echo "'\$ $make $mkargs'"
echo ""
$make $mkargs
echo "--------------------------------------------------------------------------"
