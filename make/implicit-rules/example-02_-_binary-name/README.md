# Introduction

This example shows how to build a C++-program using implicit rules. But we want
to define our own rule for linking to be able to define the name of the output
binary by our own.

# TL;DR

If you have a single source file `main.cc` or `main.cpp` use a makefile with
following content:
```
LDLIBS += -lstdc++ # or: CC=g++
.PHONY: clean
out: main.o ; $(LINK.o) $^ $(LOADLIBES) $(LDLIBS) -o $@
clean: ; $(RM) out main.o
```
or
```
.PHONY: clean
out: main.o ; $(CXX) $(LDFLAGS) $(TARGET_ARCH) $^ $(LOADLIBES) $(LDLIBS) -o $@
clean: ; $(RM) out main.o
```
Prefer the second example.

# Description

We want do define our own binary name regardless from which source file it is
build. Therefore, we have to define a explicit rule for linking the object
files to the executable:
```
out: main.o
```
There is no builtin implicit pattern rule which matches by the implicit rule
search algorithm. So, we have to define our own recipe. If we look inside the
make database (`make -p`) we find some examples we could use:
- Example 1: `$(LINK.o) $^ $(LOADLIBES) $(LDLIBS) -o $@`
- Example 2: `$(CXX) $(LDFLAGS) $(TARGET_ARCH) $^ $(LOADLIBES) $(LDLIBS) -o $@`

In both cases the object files are still created by the builtin implicit pattern
rule:
```
%.o: %.cc
    $(COMPILE.cc) $(OUTPUT_OPTION) $<

%.o: %.cpp
    $(COMPILE.cpp) $(OUTPUT_OPTION) $<

OUTPUT_OPTION = -o $@
COMPILE.cc = $(CXX) $(CXXFLAGS) $(CPPFLAGS) $(TARGET_ARCH) -c
COMPILE.cpp = $(COMPILE.cc)
```

## Example 1

The rule can be defined like this:
```
out: main.o
    $(LINK.o) $^ $(LOADLIBES) $(LDLIBS) -o $@
```
This has one little issue. We want to link object files build from C++ sources.
If we check the make database we found following definition:
```
LINK.o = $(CC) $(LDFLAGS) $(TARGET_ARCH)
```
Per default `CC` is set to the C compiler `cc`. On a GNU/Linux system this is
usually the `gcc`. If your C++ object files containing stuff from standard C++
library the linking failes. To avoid this we can change one the following
builtin implicit variables:
- Variable 1: `LDLIBS += -lstdc++`<br />
  With the variable `LDLIBS` we can set additional libraries to link into the
  executable.
- Variable 2: `CC = g++`<br />
  We set the compiler to a C++ compiler which calls the linker. On a GNU/Linux
  system we can use the `g++`. In that case the standard C++ library is linked
  per default if it is needed.

So, we have to discuss about `LDLIBS+=-lstdc++` vs `CC=g++`.

First of all, setting these variables as a command line option or globally
inside the makefile means, that all other recipe actions will use the same
setting. Keep this in mind if you will create a some more complex makefile as
shown in this example. To avoid possible issues you can use _target-specific
variables_:
```
out: LDLIBS += -lstdc++
out: main.o
    $(LINK.o) $^ $(LOADLIBES) $(LDLIBS) -o $@
```
or
```
out: CC = g++
out: main.o
    $(LINK.o) $^ $(LOADLIBES) $(LDLIBS) -o $@
```

The second thing you should care about is that in the first case the C
compiler calls the linker and in the second case the C++ compiler calls the
linker. The difference could be more than just linking other libraries per
default. So check which one is more suitable in your project.

## Example 2

The recipe in example 1 doesn't work out of the box for C++ source files. The
problem is the definition of `LINK.o` which uses the C compiler to call the
linker. Therefore, we can just combine the builtin recipe for linking object
files and a modifed version of `LINK.o`. We want to use `CXX` instead of `CC`
and we are ending up with following rule:
```
out: main.o
    $(CXX) $(LDFLAGS) $(TARGET_ARCH) $^ $(LOADLIBES) $(LDLIBS) -o $@
```

# References

- [GNU make - Implicit Rules](https://www.gnu.org/software/make/manual/html_node/Implicit-Rules.html#Implicit-Rules)
- [GNU make - Using Implicit Rules](https://www.gnu.org/software/make/manual/html_node/Using-Implicit.html#Using-Implicit)
- [GNU make - Implicit Rule Search Algorithm](https://www.gnu.org/software/make/manual/html_node/Implicit-Rule-Search.html#Implicit-Rule-Search)
- [GNU make - Implicit Variables](https://www.gnu.org/software/make/manual/html_node/Implicit-Variables.html#Implicit-Variables)
- [GNU make - Automatic Variables](https://www.gnu.org/software/make/manual/html_node/Automatic-Variables.html#Automatic-Variables)
- [O'Reilly - Variables](https://www.oreilly.com/library/view/managing-projects-with/0596006101/ch03.html)
