#!/bin/bash

# make binary to call
make='make --no-print-directory'

# make arguments
mkargs=""

echo "--------------------------------------------------------------------------"
echo "Test 1: Project 1 - Clean"
mkargs='-C project1/ clean'
echo "'\$ $make $mkargs'"
echo ""
$make $mkargs
echo "--------------------------------------------------------------------------"
echo "Test 2: Project 1 - Build"
mkargs='-C project1'
echo "'\$ $make $mkargs'"
echo ""
$make $mkargs
echo "--------------------------------------------------------------------------"
echo "Test 3: Project 1 - Rebuild after touching main.cc"
touch project1/main.cc
mkargs='-C project1'
echo "'\$ $make $mkargs'"
echo ""
$make $mkargs
echo "--------------------------------------------------------------------------"
echo "Test 4: Project 1 - Rebuild after touching print.cc"
touch project1/print.cc
mkargs='-C project1'
echo "'\$ $make $mkargs'"
echo ""
$make $mkargs
echo "--------------------------------------------------------------------------"
echo "Test 5: Project 2 - Clean"
mkargs='-C project2/ clean'
echo "'\$ $make $mkargs'"
echo ""
$make $mkargs
echo "--------------------------------------------------------------------------"
echo "Test 6: Project 2 - Build"
mkargs='-C project2'
echo "'\$ $make $mkargs'"
echo ""
$make $mkargs
echo "--------------------------------------------------------------------------"
echo "Test 7: Project 2 - Rebuild after touching main.cc"
touch project2/main.cc
mkargs='-C project2'
echo "'\$ $make $mkargs'"
echo ""
$make $mkargs
echo "--------------------------------------------------------------------------"
echo "Test 8: Project 2 - Rebuild after touching print.cc"
touch project2/print.cc
mkargs='-C project2'
echo "'\$ $make $mkargs'"
echo ""
$make $mkargs
echo "--------------------------------------------------------------------------"
echo "Test 9: Project 2 - Rebuild after touching print.h"
touch project2/print.h
mkargs='-C project2'
echo "'\$ $make $mkargs'"
echo ""
$make $mkargs
echo "--------------------------------------------------------------------------"
