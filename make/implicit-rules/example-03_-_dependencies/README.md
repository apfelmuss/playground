# Introduction

This example shows how to build a C++-program from multiple source files using
the builtin implicit rules. We have a `main.cc` from which we want to build our
executable. This main file uses some implementations from other source files.
Therefore, we have to link more object files to get our exectuable.

# TL;DR

We have a `main.cc` which uses some implementation from another source file
`print.cc`.

Using `extern` keyword in `main.cc` and not having a header file `print.h`:
```
.PHONY: clean
main: print.o
clean: ; $(RM) main print.o
```

Having a header file `print.h` which is included by `main.cc` and `print.cc`
```
.PHONY: clean
main: print.o
print.o: print.h
clean: ; $(RM) main print.o
```

# Description

We want to build our executable from different source files. Each file can be
compiled by it's own. This means from every source file we create an object
file in seperate compile steps. Afterwards the linker has to combine all these
object files to build one executable file.

Our executable should be build from `main.cc` where we call the method
`print()` implemented in `print.cc`.

The first step is to create a rule to build the executable from object files:
```
main: print.o
```
This is an ordinary rule with a prerequisite (dependency) and __no__ recipe
(not an __empty__ recipe).

The implicit pattern rule search algorithm is triggered and find the builtin
pattern rule `%: %.cc`. Our rule would be expanded to `main: main.cc print.o`.

Now, the prerequisites (`main.cc` and `print.o`) must be checked if they ought
to exist and/or other rules can be found to (re-)build them.

Therefore, the algorithm trys to figure out how to build `main.cc` and finds
the rule `%.cc:`. This rule has no recipe and no prerequisites (target only).
It expands to `main.cc:`. This means the file `main.cc` ought to exist.

Now the algorithm trys to find a rule to build `print.o`. It finds `%.o: %.cc`
and expands it into `print.o: print.cc`. There are no further rules to build
the prerequisite `print.cc` and so `print.cc` ought to exist. Therefore, the
pattern rule `%.o: %.cc` can be used to create `print.o`.

The search algorithm stops and uses the pattern rule `main: main.cc print.o`,
because the prerequisites ought to exist or can be build.

Our rule `main: print.o` will be replaced by the search algorithm with
following rules:
```
main: main.cc print.o
    $(LINK.cc) $^ $(LOADLIBES) $(LDLIBS) -o $@

print.o: print.cc
    $(COMPILE.cc) $(OUTPUT_OPTION) $<

main.cc:

print.cc:
```
Or more exactly with expanded builtin variables:
```
main: main.cc print.o
    $(CXX) $(CXXFLAGS) $(CPPFLAGS) $(LDFLAGS) $(TARGET_ARCH) $^ $(LOADLIBES) $(LDLIBS) -o $@

print.o: print.cc
    $$(CXX) $(CXXFLAGS) $(CPPFLAGS) $(OUTPUT_OPTION) $<

main.cc:

print.cc:
```

A more detailed description about the search algorithm by example can be found
in [Implicit Rules - Simplest build](../example-01_-_simplest-build/README.md).

In the `main.cc` we want to call a function `print()`, which is implemented in
`print.cc`. We want to treat each `*.cc` file as a single compile unit. This
means each file is compiled independently and separate objects for each file
will be build.

During compilation of `main.cc` the compiler has to know something about the
symbol `paint()` which we call. There are two different solutions which affects
our makefile: `extern` vs. include header file with function prototype (forward
declaration). See following examples for further descriptions.

## Example 1 - extern declaration

You can use an `extern` declaration of our function: `extern void print();`.
This tells the compiler about the symbol and its type. The compiler knows to
which symbol must be jumped and what type will be returned. It's all it needs
to compile `main.cc`. The definition/implementation of the function can be in
anohter object file. The linker will put it all together.

If you use this approach no further modifcations are needed in our makefile.
Our rule `main: print.o` fits.

Our final make file could look like following:
```
.PHONY: clean
main: print.o
clean: ; $(RM) main print.o
```
Try `make -p` to check the rules database and see by which rules our rules get
replaced.

## Example 2 - header file

A common way is to provide a header file which contains declarations of
functions and types. Other files can include this header. The preprocessor
replaces the include with the content of the file. This intermediate file is
called a translation unit. In that file all the declarations of types and
functions are placed. It's called forward declaration. The compiler now knows
about the symbols and types and can compile the current translation unit
without knowing about the definition/implemenation of the functions and types.

Using this approach means that our build depends on this header file too even
if our header file will not be compiled. But the preprocessor must re-run if
this file changes and the translation unit, containing the definition, must be
re-compiled. Also the linker must be re-run to combine the resultant object
files.

In our case the header file `print.h` looks like:
```
#pragma once

void print();
```
And the implemenation file `print.c` looks like:
```
#include "print.h"

void print() { ...  }
```

The only thing to do is to add a rule to recompile the file which implements
this function. This means `print.o` must be re-created from `print.cc` if
`print.h` changes. This can be done by adding the rule `print.o: print.h`.

Again, this is a rule with no recipe which triggers the implicit pattern rule
search algorithm. It finds the rule `%.o: %.cc` and expands it to `print.o:
print.cc print.h`. Now, the prerequisites are checked if they are exist or
ought to exist or must be created by another rule. For the prerequisite
`print.cc` the algorithm find the rule `%.cc:`, which expands into `print.cc:`.
This means the file `print.cc` ought to exist. For the prerequisite `print.h`
the algorithm find the rule `%h:`, which expands into the rule `print.h:`. This
means the file `print.h` ought to exist. The algorithm stops because of
successfully match. Our own defined rule `print.o: print.h` will be replaced
by:
```
print.o: print.cc print.h
    $(COMPILE.cc) $(OUTPUT_OPTION) $<

print.cc:

print.h:
```

We don't have to extend the rule `main: print.o` because we have already the
prerequisite `print.o` which in turn depends on `print.h`. Therefore the
linking process will be re-triggered if `print.h` changes.

Our final make file could look like following:
```
.PHONY: clean
main: print.o
print.o: print.h
clean: ; $(RM) main print.o
```
Try `make -p` to check the rules database and see by which rules our rules get
replaced.

# References

- [GNU make - Implicit Rules](https://www.gnu.org/software/make/manual/html_node/Implicit-Rules.html#Implicit-Rules)
- [GNU make - Using Implicit Rules](https://www.gnu.org/software/make/manual/html_node/Using-Implicit.html#Using-Implicit)
- [GNU make - Implicit Rule Search Algorithm](https://www.gnu.org/software/make/manual/html_node/Implicit-Rule-Search.html#Implicit-Rule-Search)
- [GNU make - Implicit Variables](https://www.gnu.org/software/make/manual/html_node/Implicit-Variables.html#Implicit-Variables)
- [GNU make - Automatic Variables](https://www.gnu.org/software/make/manual/html_node/Automatic-Variables.html#Automatic-Variables)
- [O'Reilly - Variables](https://www.oreilly.com/library/view/managing-projects-with/0596006101/ch03.html)
