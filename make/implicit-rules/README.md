# Introduction

Here I would like to show different examples about implicit rules and
variables. Some rules are already defined for many common task, like compiling
and linking C/C++-programs. The intention is not to find a solution for a given
problem. Just to show what is already there, how it works and in what kind of
situation it's useful. So, the following examples are for learning.

# Examples

- [Example 1 - Simplest C++ Build](example-01_-_simplest-build/README.md)
- [Example 2 - Output Binary Name](example-02_-_binary-name/README.md)
- [Example 3 - Dependencies](example-03_-_dependencies/README.md)

# Basics knowledge

The examples above use many special make-terms. If you know the GNU make manual
in detail you can ignore this section. Otherwise you will find here a short
summary of basic make knowledge related to implicit rules.

## Important terms

- Rule<br />
  Common shape:
  ```
  target … : prerequisites …
      recipe
      …
      …
  ```
  - A target is usually the name of a file that is generated by a program.
    (mandatory)
  - A prerequisite is a file that is used as input to create the target.
    (optional)
  - A recipe is one or more commands, normlay to _make_ the target from the
    prerequisites. (optional)

  A rule, then, explains how and when to remake certain files which are the
  targets of the particular rule. make carries out the recipe on the
  prerequisites to create or update the target. A rule can also explain how and
  when to carry out an action.

- Explicit Rule<br />
  The targets and prerequisites consist of explicit file names. The pattern
  character '%' isn't used. (`foo: foo.o bar.o ; foobar $^ $@`)

- Static Pattern Rule<br />
  It's an explicit rule with the shape:
  ```
  targets …: target-pattern: prereq-patterns …
      recipe
      …
  ```
  - The targets list specifies the targets that the rule applies to.
  - The target-pattern and prereq-patterns say how to compute the prerequisites
    of each target.

  It can also have the pattern character '%' in the target and prerequisite
  file names. But this is still an explicit rule, not a implicit rule using
  a pattern rule. The decision when the rule applies differs between these two
  rule classes.

- Implicit Rule<br />
  The rule doesn't name explicit files, it contains a pattern which matches
  files. It can be a pattern rule (using pattern character '%') or the old
  fashioned suffix rule.

  You can write your own explicit rule without an recipe. Then the implicit
  search algorithm is triggered to find a pattern rule which matches your
  explicit rule. Then the recipe from the implicit rule is used for your
  explicit rule.

  `make` comes already with many builtin implicit rules. Just run `make -p` in
  an empty folder and see what is alreday defined.

- Pattern Rule<br />
  It's an implicit rule. A pattern rule looks like an ordinary rule, except
  that its target contains the character ‘%’ (exactly one of them).
  (example: `%: %.o`)

- Suffix Rule<br />
  It's an implicit rule. This kind of rule is deprecated and it's available for
  compatibility reasons only.
  - Single suffix rule `.o:` same as pattern rule `%: %.o`
  - Double suffix rule `.c.o:` same as pattern rule `%.o: %.c`

## Important builtin implicit rules

```
LINK.cc = $(CXX) $(CXXFLAGS) $(CPPFLAGS) $(LDFLAGS) $(TARGET_ARCH)

LINK.cpp = $(LINK.cc)

LINK.o = $(CC) $(LDFLAGS) $(TARGET_ARCH)

COMPILE.cc = $(CXX) $(CXXFLAGS) $(CPPFLAGS) $(TARGET_ARCH) -c

COMPILE.cpp = $(COMPILE.cc)

OUTPUT_OPTION = -o $@

%.o:

%.cc:

%.cpp:

%: %.cc
    $(LINK.cc) $^ $(LOADLIBES) $(LDLIBS) -o $@

%: %.cpp
    $(LINK.cpp) $^ $(LOADLIBES) $(LDLIBS) -o $@

%: %.o
    $(LINK.o) $^ $(LOADLIBES) $(LDLIBS) -o $@

%.o: %.cc
    $(COMPILE.cc) $(OUTPUT_OPTION) $<

%.o: %.cpp
    $(COMPILE.cpp) $(OUTPUT_OPTION) $<
```

## Important implicit variables used by recipes in builtin implicit rules

- `CC`: Program for compiling C programs. default: ‘cc’

- `CXX`: Program for compiling C++ programs. default: ‘g++’

- `CFLAGS`: Extra flags to give to the C compiler. default: ´´

- `CXXFLAGS`: Extra flags to give to the C++ compiler. default: ’’

- `CPPFLAGS`: Extra flags to give to the C preprocessor and programs that use
  it (the C and Fortran compilers). default: ’’

- `LDFLAGS`: Extra flags to give to compilers when they are supposed to invoke
  the linker, ‘ld’, such as -L. Libraries (-lfoo) should be added to the LDLIBS
  variable instead. default: ’’.

- `TARGET_ARCH`: Not documented in GNU make manual. But most internet search
  hits say this is used for architecture-specific compilation options for
  cross-compilation. For example:<br />
  `TARGET_ARCH="-march=armv7ve+vfpv4+neon -mtune=cortex-a17"`

- `LOADLIBES`: Deprecated, see LDLIBS.

- `LDLIBS`: Library flags or names given to compilers when they are supposed to
  invoke the linker, ‘ld’. LOADLIBES is a deprecated (but still supported)
  alternative to LDLIBS. Non-library linker flags, such as -L, should go in the
  LDFLAGS variable. default: ’’

# Important automatic variables

- `@`: The file name of the target of the rule. If the target is an archive
  member, then ‘$@’ is the name of the archive file. In a pattern rule that has
  multiple targets, ‘$@’ is the name of whichever target caused the rule’s
  recipe to be run.

- `<`: The name of the first prerequisite. If the target got its recipe from an
  implicit rule, this will be the first prerequisite added by the implicit
  rule.

- `^`: The names of all the prerequisites, with spaces between them. For
  prerequisites which are archive members, only the named member is used (see
  Archives). A target has only one prerequisite on each other file it depends
  on, no matter how many times each file is listed as a prerequisite. So if you
  list a prerequisite more than once for a target, the value of $^ contains
  just one copy of the name. This list does not contain any of the order-only
  prerequisites; for those see the ‘$|’ variable.

- `?`: The names of all the prerequisites that are newer than the target, with
  spaces between them. If the target does not exist, all prerequisites will be
  included.  For prerequisites which are archive members, only the named member
  is used.

- `*`: The stem with which an implicit rule matches (see How Patterns Match).
  If the target is dir/a.foo.b and the target pattern is a.%.b then the stem is
  dir/foo. The stem is useful for constructing names of related files.

  In a static pattern rule, the stem is part of the file name that matched the
  ‘%’ in the target pattern.

## Important GNU tools

- `gcc`
  It's the GNU C Compiler from the GCC (GNU Compiler Collection).

- `g++`
  It's the GNU C++ Compiler from the GCC (GNU Compiler Collection).

- `cc`
  It's the UNIX compiler command. Which compiler is used depends on the OS
  platform. On a GNU/Linux platform this is just a symbolic link to `gcc`.

## Commands

```
make -p [<TARGET>]
```
Print the data base (rules and variable values) that results from reading
the makefiles; then execute as usual or  as  otherwise  specified.   This
also  prints  the version information given by the -v switch (see below).
To print the data base without trying to remake any files,  use  make  -p
-f/dev/null.

# References

- [GNU make - Rule Introduction](https://www.gnu.org/software/make/manual/html_node/Rule-Introduction.html)
- [GNU make - Static Pattern Rules](https://www.gnu.org/software/make/manual/html_node/Static-Pattern.html)
- [GNU make - Implicit Rules](https://www.gnu.org/software/make/manual/html_node/Implicit-Rules.html)
- [GNU make - Implicit Variables](https://www.gnu.org/software/make/manual/html_node/Implicit-Variables.html)
- [GNU make - Automatic Variables](https://www.gnu.org/software/make/manual/html_node/Automatic-Variables.html)
- [GNU make - Implicit Pattern Rules](https://www.gnu.org/software/make/manual/html_node/Pattern-Rules.html)
- [GNU make - Implicit Suffix Rules](https://www.gnu.org/software/make/manual/html_node/Suffix-Rules.html)
- [GNU make - Static pattern rules vs. implicit rules](https://www.gnu.org/software/make/manual/html_node/Static-versus-Implicit.html)
