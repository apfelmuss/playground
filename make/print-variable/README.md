# Introduction

Try to find a way to print the value of a variable.

# Quick answer

Add following rule:
```
print-% : ; @echo $* = $($*)
```
Or:
```
print-% : ; $(info $* is a $(flavor $*) variable set to '$($*)') @true
```
Call your Makefile with target `print-` and the name of the variable you
are interested in:
```
make print-VAR_NAME
```

# Description

If a Makefile grows and getting complex it gets difficult to determine the
value of a variable by reading the Makefile. Expanding a variable can debend on
many factors, like the order the Makefile is parsed, complex statements,
arithmetic and substituion. So we discuss different ways to debug the Makefile
and getting the value of a variable.

## Command line option `--debug[=FLAGS]`
### Description
### Test

## Builtin commands `$(info ...)`, `$(error ...)`
### Description
### Test

## Bash command `echo`
### Description
### Test

## Pattern Rule

### Description

Add a phony pattern rule to your Makefile:
```
.PHONY : print-%

print-% : ; @echo $* = $($*)
```

To get some more information you can use the more complex rule:
```
print-% : ; $(info $* is a $(flavor $*) variable set to '$($*)') @true
```

A rule will be executed after all variables are expanded and assigned.
Therefore, the print message shows you the last exapnding result, you did not
see interim results during the 'way' of expanding.

### Test

The first pattern rule will be demonstrated in `rule-simple` and the second
pattern rule in `rule-complex`. Both examples containing a project with a
simple C++ hello-world-program and a Makefile to build it. Just run
`rule-simple/test.sh` or `rule-complex/test.sh` to see how it works.

# Conclusion

# References

- [Stackoverflow - Print variable, simple](https://stackoverflow.com/a/25817631)
- [Stackoverflow - Print variable, complex](https://stackoverflow.com/a/34255185)
