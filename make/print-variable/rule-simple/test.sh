#!/bin/bash

# make binary to call
make='make --no-print-directory'

# make arguments
mkargs=""

echo "--------------------------------------------------------------------------"
echo "Test 1: Just make"
mkargs='-C project/'
echo "'\$ $make $mkargs'"
echo ""
$make $mkargs
echo "--------------------------------------------------------------------------"
echo "Test 2: Clean"
mkargs='-C project/ clean'
echo "'\$ $make $mkargs'"
echo ""
$make $mkargs
echo "--------------------------------------------------------------------------"
echo "Test 3: Print internal variable"
mkargs='-C project/ print-TARGET'
echo "'\$ $make $mkargs'"
echo ""
$make $mkargs
echo "--------------------------------------------------------------------------"
echo "Test 4: Print implicit variable"
mkargs='-C project/ print-RM'
echo "'\$ $make $mkargs'"
echo ""
$make $mkargs
echo "--------------------------------------------------------------------------"
echo "Test 5: Print implicit variable"
mkargs='-C project/ print-CXX'
echo "'\$ $make $mkargs'"
echo ""
$make $mkargs
echo "--------------------------------------------------------------------------"
echo "Test 6: Just make with command line arguments"
mkargs='CXX=gcc LDLIBS=-lstdc++ -C project/'
echo "'\$ $make $mkargs'"
echo ""
$make $mkargs
echo ""
mkargs='-C project/ clean'
echo "'\$ $make $mkargs'"
echo ""
$make $mkargs
echo "--------------------------------------------------------------------------"
echo "Test 7: Print make command line arguments"
mkargs='CXX=gcc LDLIBS=-lstdc++ -C project/ print-CXX'
echo "'\$ $make $mkargs'"
echo ""
$make $mkargs
echo ""
mkargs='CXX=gcc LDLIBS=-lstdc++ -C project/ print-LDLIBS'
echo "'\$ $make $mkargs'"
echo ""
$make $mkargs
echo "--------------------------------------------------------------------------"
echo "Test 8: Print not defined variable"
mkargs='-C project/ print-NOT_DEFINED'
echo "'\$ $make $mkargs'"
echo ""
$make $mkargs
echo "--------------------------------------------------------------------------"
