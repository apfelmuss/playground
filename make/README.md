# Introduction

This is a collection of different `make` topics. I try to find answers and best
or common practises for frequently asked questions. Currently there is no
specific or common structure covering all topics. Each problem or each question
is discussed in a separate subdirectory and it's treated as a sub project.
Check the correspondig reamde file to get further information about the
question itself, the structure of the sub project and the answer and
conclusion:

- [Implicit Rules](implicit-rules/README.md)
- [Print Variables](print-variable/README.md)
