# Introduction

This `playground` project is used for very detailed questions regarding to
different programming languages and utilities. It contrast different solutions
for a specific problem and tries to work out a best or a common practice.

The idea is to face one problem or one question. Then different approaches
should be tested to answer the question or to get a solution. For each
examination there should one separate readme file where the question and the
answer are discussed in detail.

How different solutions for one question can be tested depends heavily on the
problem itself and which programming language or utility is used. Therefore,
the only common structure of this project is to divide the top of this project
into different programming languages and utilities. Check the differnt readmes
inside the sub directories for furhter informations:

- [bash](bash/README.md)
- [make](make/README.md)
