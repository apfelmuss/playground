#!/bin/bash

echo "a) if-then-else with 'test ('[')"
if [ "$0" = "${BASH_SOURCE[0]}" ]; then
  echo "I'm not sourced, I have my own process"
else
  echo "I'm sourced, I run in my parents process"
fi

echo ""
echo "b) if-then-else with '[[')"
if [[ "$0" == "${BASH_SOURCE[0]}" ]]; then
  echo "I'm not sourced, I have my own process"
else
  echo "I'm sourced, I run in my parents process"
fi

echo ""
echo "c) one liner with 'test' ('[')"
[ "$0" = "${BASH_SOURCE[0]}" ] && echo "not sourced" || echo "sourced"

echo ""
echo "d) one liner '[['"
[[ "$0" == "${BASH_SOURCE[0]}" ]] && echo "not sourced" || echo "sourced"
