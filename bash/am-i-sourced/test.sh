#!/bin/bash

# default shell
shell=bash

# the first argument of the script is treated as the shell to use
[[ ! -z $1 ]] && shell=$1

echo "--------------------------------------------------------------------------"
echo "Test 1: Execute the script"
echo "'\$ $shell script/getExecutionType.sh"
echo ""
$shell script/getExecutionType.sh
echo "--------------------------------------------------------------------------"
echo "Test 2: Source the script"
echo "'\$ source script/getExecutionType.sh"
echo ""
source script/getExecutionType.sh
echo "--------------------------------------------------------------------------"
echo "Test 3: Execute the link"
echo "'\$ $shell script/getCallType.sh"
echo ""
$shell script/getCallType.sh
echo "--------------------------------------------------------------------------"
echo "Test 4: Source the link"
echo "'\$ source script/getCallType.sh"
echo ""
source script/getCallType.sh
echo "--------------------------------------------------------------------------"
