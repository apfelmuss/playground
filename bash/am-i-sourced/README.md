# Introduction

Try to find a way to determine if the running script is spawned in a new shell
process (as a child process) or if it's sourced from an active shell or inside
another script (running in same process as the parent).

# Quick answer

For Bash, no POSIX-compliant:
```
if [ "$0" = "${BASH_SOURCE[0]}" ]; then
  echo "I'm not sourced, I have my own process"
else
  echo "I'm sourced, I run in my parents process"
fi
```

As an one-liner using pattern match command (be sure first branch never fails):
```
[[ "$0" == "${BASH_SOURCE[0]}" ]] && echo "not sourced" || echo "sourced"
```

# Description

A POSIX-shell like `sh` has an environment variable `$0` which holds the name
of the script with the relative path to it from location of invocation.

The GNU Bash has an array variable `$BASH_SOURCE` where the first entry holds
the name of the script with the relative path to it from the location of
invocation.

There a different possibilities how the script was invoked:
- `bash <SCRIPT>`
- `source <SCRIPT>`
- Through a symbolic link with a different name

# Test

There is an example script `script/getExecutionType.sh` where different
approaches are used to determine if a script is sourced or not.  There is also a
symbolic link to it `script/getCallType.sh`, but the name of the link differs
from the script name.

Just run `test.sh` to see the different results. Per default the `bash` is used
to execute the example script. With the first argument you can pass another
shell like `test.sh sh`.

# Conclusion

The variables `$0` and `${BASH_SOURCE[0]}` returning the name of the script
with the relative path to it, from the location where the script was invoked.

But if the script is invoked via `source`, `$0` returns always the name of the
source itself (parent process), no matter what is sourced.

To get always the script name (with relative path to it), regardless how it is
invoked, use: `${BASH_SOURCE[0]}`. Keep in mind, this works only in the bash
and not in a POSIX compliant shell like `sh`.

Therefore, we can just compare this two variables to figure out if a script is
sourced or not.

**So if you are inside a bash script and you don't need to be POSIX compliant
use:**

A `if-then-else` always works, regardless of using `[` or `[[` for comparison.
```
if [ "$0" = "${BASH_SOURCE[0]}" ]; then
  echo "I'm executed, I have my own process"
else
  echo "I'm sourced, I run in my parents process"
fi
```

Also, you can use a one-liner using `&&` and `||`. But keep in mind, it's not
the same as `if-then-else`. If any command from the left side of `||` fails,
the right side will be executed.
```
[[ "$0" == "${BASH_SOURCE[0]}" ]] && echo "not sourced" || echo "sourced"
```

# References

- [sh](https://pubs.opengroup.org/onlinepubs/9699919799/utilities/sh.html)
- [sh command language - Special parameters](https://pubs.opengroup.org/onlinepubs/9699919799/utilities/V3_chap02.html#tag_18_05_02)
- [sh - source](https://pubs.opengroup.org/onlinepubs/9699919799/utilities/ex.html#tag_20_40_13_39)
- [sh - Invoke Shell](https://pubs.opengroup.org/onlinepubs/9699919799/utilities/mailx.html#tag_20_75_13_35)
- [GNU Bash - Special parameters](https://www.gnu.org/software/bash/manual/bash.html#Special-Parameters)
- [GNU Bash - Bash variables](https://www.gnu.org/software/bash/manual/bash.html#Bash-Variables)
- [GNU Bash - Builtins](https://www.gnu.org/software/bash/manual/bash.html#Bourne-Shell-Builtins)
- [GNU Bash - Invoking Bash](https://www.gnu.org/software/bash/manual/bash.html#Invoking-Bash)
