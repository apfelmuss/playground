# Introduction

Try to find a way how to get the name of a shell script inside the script
itself.

# Quick answer

For Bash, no POSIX-compliant:
```
scriptName="$( basename -- "$( readlink -f -- "${BASH_SOURCE[0]}" )" )"
```

# Description

A POSIX-shell like `sh` has an environment variable `$0` which holds the name
of the script with the relative path to it from location of invocation.

The GNU Bash has an array variable `$BASH_SOURCE` where the first entry holds
the name of the script with the relative path to it from the location of
invocation.

There a different possibilities how the script was invoked:
- `bash <SCRIPT>`
- `source <SCRIPT>`
- Through a symbolic link with a different name

# Test

There is an example script `script/getname.sh` where different approaches are
used to get the name of the script file. There is also a symbolic link to it
`script/getNAME.sh`, but the name of the link differs from the script name.

Just run `test.sh` to see the different results. Per default the `bash` is used
to execute the example script. With the first argument you can pass another
shell like `test.sh sh`.

# Conclusion

The variables `$0` and `${BASH_SOURCE[0]}` returning the name of the script
with the relative path to it, from the location where the script was invoked.

With `basename` you can remove the path from the given file and get the file
name. It's the string after the last `/`.

This works if the script was invoked by `./<path>/<script>` or `bash
<path>/<script>`

But if the script is invoked via `source`, `$0` returns always the name of the
source itself (parent process), no matter what is sourced.

--> To get always the script name (with relative path to it), regardless how it
is invoked, use: `${BASH_SOURCE[0]}`. Keep in mind, this works only in the bash
and not in a POSIX compliant shell like `sh`.

But a symbolic link isn't resolved. You get the name to the link, not the file
name to which the link points to.

--> Use `readlink -f` to resovle the link. This is also not a POSIX utility.

**So if you are inside a bash script and you don't need to be POSIX compliant
use:**
```
scriptName="$( basename -- "$( readlink -f -- "${BASH_SOURCE[0]}" )" )"
```

# References

- [sh](https://pubs.opengroup.org/onlinepubs/9699919799/utilities/sh.html)
- [sh command language - Special parameters](https://pubs.opengroup.org/onlinepubs/9699919799/utilities/V3_chap02.html#tag_18_05_02)
- [GNU Bash - Special parameters](https://www.gnu.org/software/bash/manual/bash.html#Special-Parameters)
- [GNU Bash - Bash variables](https://www.gnu.org/software/bash/manual/bash.html#Bash-Variables)
- [Open Group - basename](https://pubs.opengroup.org/onlinepubs/9699919799/functions/basename.html)
- [GNU Coreutils - readlink](https://www.gnu.org/software/coreutils/manual/html_node/readlink-invocation.html)
- [Stackoverflow](https://stackoverflow.com/a/29835459)
