#!/bin/bash

sol1="$( basename -- "$0" )"
sol2="$( basename -- "${BASH_SOURCE[0]}" )"
sol3="$( basename -- "$( readlink -f -- "$0" )" )"
sol4="$( basename -- "$( readlink -f -- "${BASH_SOURCE[0]}" )" )"

echo '"$0"'"                                                         = $0"
echo '"${BASH_SOURCE[0]}"'"                                          = ${BASH_SOURCE[0]}"
echo '"$( basename -- "$0" )"'"                                      = $sol1"
echo '"$( basename -- "${BASH_SOURCE[0]}" )"'"                       = $sol2"
echo '"$( basename -- "$( readlink -f -- "$0" )" )"'"                = $sol3"
echo '"$( basename -- "$( readlink -f -- "${BASH_SOURCE[0]}" )" )"'" = $sol4"
