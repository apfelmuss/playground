#!/bin/bash

#
# get script name
#
# Here we want the name of the symbolic link, not the name of the script
# itself. Therefore, 'readlink' isn't used.
#
# for POSIX compliance use ${0##*/}
#
name="$( basename -- "${BASH_SOURCE[0]}" )"

#
# how to quit the script
#
# Remove this for POSIX compliance and decide if your script should be
# called or sourced.
#
[[ "$0" == "${BASH_SOURCE[0]}" ]] && exitCommand=exit || exitCommand=return

#
# set usage
#
function usage
{
    cat << END
Usage: $name [-y | --ypsilon <value>] [-v | --verbose] [-h | --help] wye zed
  Operands:
  wye          first positional argument
  zed          second positional argument

  Options:
  -y | --ypsilon <value>   set a value for y
  -v | --verbose           verbose
  -h | --help              help
END
}

#
# define options
#
optstring="y:vh"
longoptstring="ypsilon:,verbose,help"

#
# re-organize command-line arguments
#
# Options inside given string '-- "$@"' will be reformatted into canonical form.
# All known options will be placed in front of the result string followed by
# the end tag '--' and remaining string with unknown options or positional
# arguments.
#
args=$(getopt -n "$name" -o "$optstring" -l "$longoptstring" -- "$@")
[[ $? -ne 0 ]] && $exitCommand 1

echo "bash command-line arguments: $@"
echo "getopt arguments: $args"

#
# set $args as new command-line arguments
#
eval set -- "$args" && unset args

echo "new bash command-line arguments: $@"

#
# set default value for the options
#
ypsilon=""
verbose=false

#
# parse string with command-line arguments and get the options
#
while true; do
    case "$1" in
        -y | --ypsilon ) ypsilon="$2"; shift 2; continue ;;
        -v | --verbose ) verbose=true; shift; continue ;;
        -h | --help    ) usage; $exitCommand 0 ;;
        --             ) shift; break ;; # end of options part from command line argument string
        *              ) echo "Error: Internal getopt error"; $exitCommand 1 ;;
    esac
done

echo "remaining bash command-line arguments: $@"

#
# if positional arguments are mandatory, check if they are set (beeing null is still possible)
# check each explicitly ...
# if [ -z "${1+x}" ]; then echo Error: Operand 'wye' is missing; usage; $exitCommand 1; fi
# if [ -z "${2+x}" ]; then echo Error: Operand 'zed' is missing; usage; $exitCommand 1; fi
# ... or just check expected number
# if [ $# -lt 2 ]; then echo Error: Operand is missing; usage; $exitCommand 1; fi
#
# if positional arguemnts aren't mandatory set a default value if they are unset
# wye=${1=wye}
# zed=${2=zed}
#
# if positional arguemnts aren't mandatory set a default value if they are unset or set and null
# wye=${1:=wye}
# zed=${2:=zed}

#
# get remaining positional arguments
#
wye=$1
zed=$2

#
# place checks here:
# - values for options with arguments
# - values for positional arguments
#
# if [ -z "${zed}" ]; then echo Error: Operand 'zed' is null; usage; $exitCommand 1; fi
# if [ -z "${wye}" ]; then echo Error: Operand 'wye' is null; usage; $exitCommand 1; fi
# if [ ! $(echo $var | grep -E "^[[:digit:]]{1,}$") ]; then echo Error: only digits for x allowed; usage; $exitCommand 1; fi

#
# start your script
#
echo "y   = $ypsilon"
echo "v   = $verbose"
echo "wye = $wye"
echo "zed = $zed"

$exitCommand 0
