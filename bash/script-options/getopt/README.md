# Introduction

This topic discuss the possibility to parse optional arguments to a shell
script using `getopt` from `util-linux` packet.

# Quick Answer

Use `getopt` when:
- need for optional arguments
- want to be able to use long options (`--myoption`)
- want to use options and operands (positional arguments) in any order
- optional option argument
- user should interact with the script
- script must not be POSIX compliant

Short overview how to use:

- define an usage printout
- define an strings (short + long)
- parse shell arguments (`"$@"`) and rearrange them by using `getopt`
- make the rearranged string as the new shell arguments string
- define default option values
- loop over all known option arguments
- get remaining operands / positional arguments
- add checks

See detailed usage in `script/getopt.sh`.

Instead of using operands you can use options with arguments and make them
mandatory or give them default values. This reduces some boiler plate code.

# Description

For a long time each FAQ, tutorial, guide, ..., told you not to use `getopt`
because of different implemenations, issues in the implementations (not able to
handle options with arguments containing whitespaces) and missing POSIX
standard.

But since 2012 there is an enhanced version of it, shipped with the
`util-linux` package directly from the linux kernel team. Using short options
are fully supported as they are described by the Opengroup. So, the behavior is
the same as the shell builtin command `getopts`. But you get two extensions.
You can use long options (double dash followed by the option string) and you
can place operands / positional arguments between the options. (Furthermore,
you can make an optional option argument;-)

The remaining arguemnt against using `getopt` is the missing POSIX
standard, and therefore it's not a shell builtin. Your system must provide this
package. So, you have to decide on which system your script must be running
(Unix, embedded Linux, ...).

The `getopt` is able to parse any argument list. If you want to parse the shell
arguments use `"$@"`. Thie idea is to rearrange the arguments and return a
result string. It means all known options (short + long) will be placed at the
front of the result string. Then a double dash is placed to mark the end of the
option part. All remaining arguments are put after the double dash into the
result string. This arguments are treated as operands / positional arguments.
Wrong user inputs (unknown options or missing option argument) are handled by
`getopt`.

This result string must be set as the new shell argument string by using `eval
set`. Than you can loop over `$1`, `$2`, ... getting the option values and
shift them out. If you reach `--` shift it out and break the loop. Only
operands / positional arguments remain in the shell argument string (`"$@"`).

Don't forget to define default values for the options. Options without an
argument are also known as flags. Use `true` or `false` as the value of the
flag. Both are commands itself and are easy to use in conditions or in a AND/OR
command list.

Add checks for options with arguments and operands to catch faulty user inputs.

The `getopt` command can handle:
- options
  - dash + single character (short option)
  - double dash + string (long option)
  - unordered
  - grouped together (only for short options)
  - unknown options
  - flags
  - with option arguemnt (value)
    - string without whitespace
    - string with whitespace (quoted with `"` or `'`)
    - empty string ( `""` or `''`)
    - optional argument
- operands / positional arguments
  - string without whitespace
  - string with whitespace (quoted with `"` or `'`)
  - empty string ( `""` or `''`)
  - interleaved with options

We have to add own code for unset or optional operands. See comments in
`script/getopt.sh` for different examples how to check for unset operands or
examples how to handle optionals operands if they aren't set or null.

You can run `test.sh` which invokes `script/getopt.sh` with different short
options and operands which tries to cover all possibilities listed above. It's
the same test set as for `getopts` (`../getopts/test.sh`) to see the behavior
of using short options is the same.

There is another test set `test-longoptions.sh` which tries to cover extra
features provided by `getopt` (double dashed long options and interleaved
operands and options).

Per default the `bash` is used to execute the example script. With the first
argument you can pass another shell like `test.sh sh`.

# References

- [util-linux - getopt](https://www.man7.org/linux/man-pages/man1/getopt.1.html)
- [util-linux - sources](https://mirrors.edge.kernel.org/pub/linux/utils/util-linux/)
- [Stackexchange](https://unix.stackexchange.com/a/62961)
- [Stackoverflow](https://stackoverflow.com/a/14203146)
