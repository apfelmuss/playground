#!/bin/bash

# default shell
shell=bash

# the first argument of the script is treated as the shell to use
[[ ! -z $1 ]] && shell=$1

echo "--------------------------------------------------------------------------"
echo "Test 1: Execute the script"
echo ""
echo "\$ $shell script/getopt.sh -h"
$shell script/getopt.sh -h
echo "--------------------------------------------------------------------------"
echo "Test 2: Source the script"
echo ""
echo "\$ source script/getopt.sh -h"
source script/getopt.sh -h
echo "--------------------------------------------------------------------------"
echo "Test 3: Execute the link"
echo ""
echo "\$ $shell script/getOPT.sh -h"
$shell script/getOPT.sh -h
echo "--------------------------------------------------------------------------"
echo "Test 4: Source the link"
echo ""
echo "\$ source script/getOPT.sh -h"
source script/getOPT.sh -h
echo "--------------------------------------------------------------------------"
echo "Test 5.1: Options"
echo ""
echo "\$ $shell script/getopt.sh -v"
$shell script/getopt.sh -v
echo "--------------------------------------------------------------------------"
echo "Test 5.2: Options"
echo ""
echo "\$ $shell script/getopt.sh -v -y ex"
$shell script/getopt.sh -v -y ex
echo "--------------------------------------------------------------------------"
echo "Test 5.2: Options"
echo ""
echo "\$ $shell script/getopt.sh -y ex -v"
$shell script/getopt.sh -y ex -v
echo "--------------------------------------------------------------------------"
echo "Test 5.4: Options"
echo ""
echo "\$ $shell script/getopt.sh -vy ex"
$shell script/getopt.sh -vy ex
echo "--------------------------------------------------------------------------"
echo "Test 5.5: Options"
echo ""
echo "\$ $shell script/getopt.sh -vyex"
$shell script/getopt.sh -vyex
echo "--------------------------------------------------------------------------"
echo "Test 5.6: Options"
echo ""
echo "\$ $shell script/getopt.sh -y \"deus ex\""
$shell script/getopt.sh -y "deus ex"
echo "--------------------------------------------------------------------------"
echo "Test 5.7: Options"
echo ""
echo "\$ $shell script/getopt.sh -y \"\""
$shell script/getopt.sh -y ""
echo "--------------------------------------------------------------------------"
echo "Test 5.8: Options"
echo ""
echo "\$ $shell script/getopt.sh -y"
$shell script/getopt.sh -y
echo "--------------------------------------------------------------------------"
echo "Test 5.9: Options"
echo ""
echo "\$ $shell script/getopt.sh -v -b"
$shell script/getopt.sh -v -b
echo "--------------------------------------------------------------------------"
echo "Test 6.1: Operands"
echo ""
echo "\$ $shell script/getopt.sh wye zed"
$shell script/getopt.sh wye zed
echo "--------------------------------------------------------------------------"
echo "Test 6.2: Operands"
echo ""
echo "\$ $shell script/getopt.sh \"w ye\" \"\""
$shell script/getopt.sh "w ye" ""
echo "--------------------------------------------------------------------------"
echo "Test 7.1: Options and Operands"
echo ""
echo "\$ $shell script/getopt.sh -v -y1 wye zed"
$shell script/getopt.sh -v -y1 wye zed
echo "--------------------------------------------------------------------------"
echo "Test 7.2: Options and Operands"
echo ""
echo "\$ $shell script/getopt.sh -v -y1 -- -wye -zed"
$shell script/getopt.sh -v -y1 -- -wye -zed
echo "--------------------------------------------------------------------------"
