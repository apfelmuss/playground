#!/bin/bash

# default shell
shell=bash

# the first argument of the script is treated as the shell to use
[[ ! -z $1 ]] && shell=$1

echo "--------------------------------------------------------------------------"
echo "Test 1: Execute the script"
echo ""
echo "\$ $shell script/getopt.sh --help"
$shell script/getopt.sh --help
echo "--------------------------------------------------------------------------"
echo "Test 2: Source the script"
echo ""
echo "\$ source script/getopt.sh --help"
source script/getopt.sh --help
echo "--------------------------------------------------------------------------"
echo "Test 3: Execute the link"
echo ""
echo "\$ $shell script/getOPT.sh --help"
$shell script/getOPT.sh --help
echo "--------------------------------------------------------------------------"
echo "Test 4: Source the link"
echo ""
echo "\$ source script/getOPT.sh --help"
source script/getOPT.sh --help
echo "--------------------------------------------------------------------------"
echo "Test 5.1: Options"
echo ""
echo "\$ $shell script/getopt.sh --verbose"
$shell script/getopt.sh --verbose
echo "--------------------------------------------------------------------------"
echo "Test 5.2: Options"
echo ""
echo "\$ $shell script/getopt.sh --verbose --ypsilon ex"
$shell script/getopt.sh --verbose --ypsilon ex
echo "--------------------------------------------------------------------------"
echo "Test 5.2: Options"
echo ""
echo "\$ $shell script/getopt.sh -y ex -v"
$shell script/getopt.sh --ypsilon ex --verbose
echo "--------------------------------------------------------------------------"
echo "Test 5.3: Options"
echo ""
echo "\$ $shell script/getopt.sh --ypsilon \"deus ex\""
$shell script/getopt.sh --ypsilon "deus ex"
echo "--------------------------------------------------------------------------"
echo "Test 5.4: Options"
echo ""
echo "\$ $shell script/getopt.sh --ypsilon \"\""
$shell script/getopt.sh --ypsilon ""
echo "--------------------------------------------------------------------------"
echo "Test 5.5: Options"
echo ""
echo "\$ $shell script/getopt.sh --ypsilon"
$shell script/getopt.sh --ypsilon
echo "--------------------------------------------------------------------------"
echo "Test 5.6: Options"
echo ""
echo "\$ $shell script/getopt.sh --verbose --ypsilon2"
$shell script/getopt.sh --verbose --ypsilon2
echo "--------------------------------------------------------------------------"
echo "Test 6.1: Operands"
echo ""
echo "\$ $shell script/getopt.sh wye zed"
$shell script/getopt.sh wye zed
echo "--------------------------------------------------------------------------"
echo "Test 6.2: Operands"
echo ""
echo "\$ $shell script/getopt.sh \"w ye\" \"\""
$shell script/getopt.sh "w ye" ""
echo "--------------------------------------------------------------------------"
echo "Test 7.1: Options and Operands"
echo ""
echo "\$ $shell script/getopt.sh --verbose --ypsilon 0 wye zed"
$shell script/getopt.sh --verbose --ypsilon 0 wye zed
echo "--------------------------------------------------------------------------"
echo "Test 7.2: Options and Operands"
echo ""
echo "\$ $shell script/getopt.sh --verbose --ypsilon 0 -- -wye -zed"
$shell script/getopt.sh --verbose --ypsilon 0 -- -wye -zed
echo "--------------------------------------------------------------------------"
echo "Test 7.3: Options and Operands"
echo ""
echo "\$ $shell script/getopt.sh wye zed --verbose"
$shell script/getopt.sh wye zed --verbose
echo "--------------------------------------------------------------------------"
echo "Test 7.4: Options and Operands"
echo ""
echo "\$ $shell script/getopt.sh wye --ypsilon 0 zed --verbose "
$shell script/getopt.sh wye --ypsilon 0 zed --verbose
echo "--------------------------------------------------------------------------"
echo "Test 7.5: Options and Operands"
echo ""
echo "\$ $shell script/getopt.sh wye zed -vy0"
$shell script/getopt.sh wye zed -vy0
echo "--------------------------------------------------------------------------"
