#!/bin/bash

# default shell
shell=bash

# the first argument of the script is treated as the shell to use
[[ ! -z $1 ]] && shell=$1

echo "--------------------------------------------------------------------------"
echo "Test 1: Execute the script"
echo ""
echo "\$ $shell script/getopts.sh -h"
$shell script/getopts.sh -h
echo "--------------------------------------------------------------------------"
echo "Test 2: Source the script"
echo ""
echo "\$ source script/getopts.sh -h"
source script/getopts.sh -h
echo "--------------------------------------------------------------------------"
echo "Test 3: Execute the link"
echo ""
echo "\$ $shell script/getOPTS.sh -h"
$shell script/getOPTS.sh -h
echo "--------------------------------------------------------------------------"
echo "Test 4: Source the link"
echo ""
echo "\$ source script/getOPTS.sh -h"
source script/getOPTS.sh -h
echo "--------------------------------------------------------------------------"
echo "Test 5.1: Options"
echo ""
echo "\$ $shell script/getopts.sh -v"
$shell script/getopts.sh -v
echo "--------------------------------------------------------------------------"
echo "Test 5.2: Options"
echo ""
echo "\$ $shell script/getopts.sh -v -x ex"
$shell script/getopts.sh -v -x ex
echo "--------------------------------------------------------------------------"
echo "Test 5.2: Options"
echo ""
echo "\$ $shell script/getopts.sh -x ex -v"
$shell script/getopts.sh -x ex -v
echo "--------------------------------------------------------------------------"
echo "Test 5.4: Options"
echo ""
echo "\$ $shell script/getopts.sh -vx ex"
$shell script/getopts.sh -vx ex
echo "--------------------------------------------------------------------------"
echo "Test 5.5: Options"
echo ""
echo "\$ $shell script/getopts.sh -vxex"
$shell script/getopts.sh -vxex
echo "--------------------------------------------------------------------------"
echo "Test 5.6: Options"
echo ""
echo "\$ $shell script/getopts.sh -x \"deus ex\""
$shell script/getopts.sh -x "deus ex"
echo "--------------------------------------------------------------------------"
echo "Test 5.7: Options"
echo ""
echo "\$ $shell script/getopts.sh -x \"\""
$shell script/getopts.sh -x ""
echo "--------------------------------------------------------------------------"
echo "Test 5.8: Options"
echo ""
echo "\$ $shell script/getopts.sh -x"
$shell script/getopts.sh -x
echo "--------------------------------------------------------------------------"
echo "Test 5.9: Options"
echo ""
echo "\$ $shell script/getopts.sh -v -b"
$shell script/getopts.sh -v -b
echo "--------------------------------------------------------------------------"
echo "Test 6.1: Operands"
echo ""
echo "\$ $shell script/getopts.sh wye zed"
$shell script/getopts.sh wye zed
echo "--------------------------------------------------------------------------"
echo "Test 6.2: Operands"
echo ""
echo "\$ $shell script/getopts.sh \"w ye\" \"\""
$shell script/getopts.sh "w ye" ""
echo "--------------------------------------------------------------------------"
echo "Test 7.1: Options and Operands"
echo ""
echo "\$ $shell script/getopts.sh -v -x1 wye zed"
$shell script/getopts.sh -v -x1 wye zed
echo "--------------------------------------------------------------------------"
echo "Test 7.2: Options and Operands"
echo ""
echo "\$ $shell script/getopts.sh -v -x1 -- -wye -zed"
$shell script/getopts.sh -v -x1 -- -wye -zed
echo "--------------------------------------------------------------------------"
