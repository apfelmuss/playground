# Introduction

This topic discuss the possibility to parse optional arguments to a shell
script using builtin command `getopts`.

# Quick Answer

Use `getopts` when:
- need for optional arguments
- user should interact with the script
- script must be POSIX compliant

Short overview how to use:
- define an usage printout
- define an option string
- define default values
- get options by parsing the shell arguemnts (`"$@"`) with `getopts`
- get remaining operands / positional arguments
- add checks

See detailed usage in `script/getopts.sh`.

Instead of using operands you can use options with arguments and make them
mandatory or give them default values. This reduces some boiler plate code.

# Description

The Opengroup has syntax guidelines for invoking utilities and scripts with
arguments (parameters) where some of them should be treated as options and
others as operands (positional arguments):
```
utility_name [-a] [-b] [-c option_argument]
    [-d|-e] [-f[option_argument]] [operand...]
```
We can see that the options must precede the operands.

The builtin command `getopts` is able to parse an argument list. Per default
the shell arguments (`"$@"`) are used for parsing. But you can parse your own
list too. Parsing is done with a simple while loop. You have to define an
option string first. Here you define the option character to use and if the
option has an argument itself. Wrong user inputs (unknown options or missing
option argument) are handled by `getopts`. Don't forget to define default
values for the options.

Options without an argument are also known as flags. Use `true` or `false` as
the value of the flag. Both are commands itself and are easy to use in
conditions or in a AND/OR command list.

If you have operands (positional arguemnts) you have to shift out the options.
After that the shell argument list (`"$@"`) contains only the operands and it's
easy to parse them.

You can reduce some boiler plate code if you handle the operands as options
too especially when user is allowed to omit them by invocation. In that case
add also default values.

Add checks for options with arguemnts and operands to catch faulty user inputs.

The builtin command `getopts` is part of the POSIX standard and therefore
available in a wide range of Unix and Linux systems.

The `getopts` command can handle:
- options
  - single character
  - unordered
  - grouped together
  - unknown options
  - flags
  - with option arguemnt (value)
    - string without whitespace
    - string with whitespace (quoted with `"` or `'`)
    - empty string ( `""` or `''`)

Remaining arguments after options can be:
- operands / positional arguments
  - string without whitespace
  - string with whitespace (quoted with `"` or `'`)
  - empty string ( `""` or `''`)

We have to add own code for unset or optional operands. See comments in
`script/getopts.sh` for different examples how to check for unset operands or
examples how to handle optionals operands if they aren't set or null.

You can run `test.sh` which invokes `script/getopts.sh` with different options
and tries to cover all possibilities listed above. There is also a symbolic
link to it `script/getOPTS.sh`, but the name of the link differs from the
script name.

Per default the `bash` is used to execute the example script. With the first
argument you can pass another shell like `test.sh sh`.

# References

- [Opengroup - getopts](https://pubs.opengroup.org/onlinepubs/9699919799/utilities/getopts.html#)
- [Opengroup - Argument Syntax](https://pubs.opengroup.org/onlinepubs/9699919799/basedefs/V1_chap12.html)
- [Opengroup - Parameter Expansion](https://pubs.opengroup.org/onlinepubs/9699919799/utilities/V3_chap02.html#tag_18_06_02)
- [GNU Bash - Shell Builtin Commands](https://www.gnu.org/software/bash/manual/bash.html#Shell-Builtin-Commands)
- [Bash Hackers](https://wiki.bash-hackers.org/howto/getopts_tutorial)
- [Stackexchange](https://unix.stackexchange.com/a/62961)
- [Stackoverflow](https://stackoverflow.com/a/14203146)
