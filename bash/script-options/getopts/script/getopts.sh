#!/bin/bash

#
# get script name
#
# Here we want the name of the symbolic link, not the name of the script
# itself. Therefore, 'readlink' isn't used.
#
# for POSIX compliance use ${0##*/}
#
name="$( basename -- "${BASH_SOURCE[0]}" )"

#
# how to quit the script
#
# Remove this for POSIX compliance and decide if your script should be
# called or sourced.
#
[[ "$0" == "${BASH_SOURCE[0]}" ]] && exitCommand=exit || exitCommand=return

#
# set usage
#
function usage
{
    cat << END
Usage: $name [-x <value>] [-v] [-h] wye zed
  Operands:
  wye          first positional argument
  zed          second positional argument

  Options:
  -x <value>   set a value for x
  -v           verbose
  -h           help
END
}

#
# define options
#
optstring="x:vh"
#
# start the option string with a colon to disable the auto error message and
# use your own message in the while loop.
# optstring=":x:vh"

#
# set default value for the options
#
x=0
verbose=false # false/true are commands and easy to use in conditions

#
# parse the shell arguments "$@" for options
#
OPTIND=1 # just to be sure to start with the first arguemnt (should be automatically set to 1)
while getopts $optstring opt "$@"; do
    case "$opt" in
        x ) x="$OPTARG"; ;;
        v ) verbose=true; ;;
        h ) usage; $exitCommand 0; ;;
        ? ) usage; $exitCommand 1; ;;
    esac
done
# remove all options from shell arguments to get operands / positional arguments only
shift $((OPTIND-1))

#
# if positional arguments are mandatory, check if they are set (beeing null is still possible)
# check each explicitly ...
# if [ -z "${1+x}" ]; then echo Error: Operand 'wye' is missing; usage; $exitCommand 1; fi
# if [ -z "${2+x}" ]; then echo Error: Operand 'zed' is missing; usage; $exitCommand 1; fi
# ... or just check expected number
# if [ $# -lt 2 ]; then echo Error: Operand is missing; usage; $exitCommand 1; fi
#
# if positional arguemnts aren't mandatory set a default value if they are unset
# wye=${1=wye}
# zed=${2=zed}
#
# if positional arguemnts aren't mandatory set a default value if they are unset or set and null
# wye=${1:=wye}
# zed=${2:=zed}

#
# get remaining positional arguments
#
wye=$1
zed=$2

#
# place checks here:
# - values for options with arguments
# - values for positional arguments
#
# if [ -z "${zed}" ]; then echo Error: Operand 'zed' is null; usage; $exitCommand 1; fi
# if [ -z "${wye}" ]; then echo Error: Operand 'wye' is null; usage; $exitCommand 1; fi
# if [ ! $(echo $var | grep -E "^[[:digit:]]{1,}$") ]; then echo Error: only digits for x allowed; usage; $exitCommand 1; fi

#
# start your script
#
echo "x   = $x"
echo "v   = $verbose"
echo "wye = $wye"
echo "zed = $zed"

$exitCommand 0
