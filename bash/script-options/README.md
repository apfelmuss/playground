# Introduction

If you want to parse some options to a shell script you can use one of the
following three approaches:
- [Environment Variables](environment/README.md)
- [Shell Arguments](arguments/README.md)
- [Command getopts](getopts/README.md)
- [Command getopt](getopt/README.md)

Using environment variables or shell arguments are easy to use. For simple and
small scripts they might be a good choice. But they aren't flexible and often
less maintainable as using the builtin command `getopts`. And if you want to
get more readability you can use `getopt` from `util-linux` if your target
system is Linux only, not a Unix.
