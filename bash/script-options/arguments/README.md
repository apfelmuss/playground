# Introduction

Discussion about parsing options to a shell script by using shell arguments.

# Quick answer

Access arguments inside the script with `$1`, `$2`, ..., `$N` or `"$@"`.

Use arguments in automated systems or simple scripts. For user scripts use
builtin command `getopts` or `getopt` from `util-linux`, especially if
arguments should be optional.

# Description

If you invoke a script, the string after the script name, till the end of the
line, is treated as arguemnts. The string is splitted by whitespaces. Each
substring is an argument: `myScript.sh arg1 arg2 arg3`. The whitespaces are the
delimiters for the parser to distinguish between different arguments.

The tricky thing is if you want to parse an argument which consist itself by
whitespaces. Such an argument must be quoted with double quotes or single quotes:
`myScript.sh arg1 arg2 "arg31 arg32"` or `myScript.sh arg1 arg2 'arg31 arg32'`.

If you are wondering why someone should use spaces inside arguments, imagine
you want to write a script, where you want to parse files and paths to it. You
could ask the same question, but ... ;-)

Inside the script we can access these arguments using positional parameters
`$1`, `$2`, ..., `$N`. If you want to treat the arguments as a list, in order
to iterate through or to parse them elsewhere, you came to different syntaxes:
`$*`, `"$*"`, `$@` and `"$@"`. The four different syntaxes results in the same
as long as you use arguments without whitespaces in it. And here we are again
with annoying arguments containg whitespaces.

You can run `test.sh` which sets some different arguments and invokes
`script/getarguments.sh`. This script tries to iterate through all arguments
in different ways and print each detected shell argument. There is also a
symbolic link to it `script/getARGUMENTS.sh`, but the name of the link differs
from the script name.

Per default the `bash` is used to execute the example script. With the first
argument you can pass another shell like `test.sh sh`.

# Conclusion

- positional parameters
  - works as expected
  - --> use this for direct access to a single arguemnt

- `$*` and `$@`
  - ignoring the quoted arguments
  - each whitespace is treated as a delimiter regardless of existing quotes

- `"$*"`
  - it's only a single word
  - expands to an empty string if no arguments are given

- `"$@"`
  - is equivalent to `$1`, `$2`, ..., `$N`
  - expands to nothing if no arguments are given
  - --> use this to get a list where you can iterate through

Limitations:
- no optional argument possible, must be at least an empty string (`""` or
  `''`)
- fixed position

Hint:
- Parsing arguments to shell functions are working in the same way.

Use it for automated systems to be sure to parse always the right arguments.
Don't try to implement your own argument parsing algorithm. Take a look at
`getopts` or `getopt`. Use one of them for a single script which should be
invoked by an user.

# References

- [Opengroup - sh](https://pubs.opengroup.org/onlinepubs/9699919799/utilities/sh.html)
- [sh command language - Special parameters](https://pubs.opengroup.org/onlinepubs/9699919799/utilities/V3_chap02.html#tag_18_05_02)
- [GNU Bash - Special Parameters](https://www.gnu.org/software/bash/manual/bash.html#Special-Parameters)
- [GNU Bash - Positional Parameters](https://www.gnu.org/software/bash/manual/bash.html#Positional-Parameters)
- [Stackoverflow](https://stackoverflow.com/a/255913)
- [Stackoverflow](https://stackoverflow.com/a/256225)
