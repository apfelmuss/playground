#!/bin/bash

echo 'Positional arguemnts $1 $2 $3 ...'
echo '$1 = '$1
echo '$2 = '$2
echo '$3 = '$3
echo '$4 = '$4

echo ""
i=0
echo 'Iterate through $*'
for a in $*; do
    echo loop entry $((++i)), element = $a;
done

echo ""
i=0
echo 'Iterate through "$*"'
for a in "$*"; do
    echo loop entry $((++i)), element = $a;
done

echo ""
i=0
echo 'Iterate through $@'
for a in $@; do
    echo loop entry $((++i)), element = $a;
done

echo ""
i=0
echo 'Iterate through "$@"'
for a in "$@"; do
    echo loop entry $((++i)), element = $a;
done
