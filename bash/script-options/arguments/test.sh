#!/bin/bash

# default shell
shell=bash

# the first argument of the script is treated as the shell to use
[[ ! -z $1 ]] && shell=$1

echo "--------------------------------------------------------------------------"
echo "Test 1: Execute the script"
echo $shell 'script/getarguments.sh arg1 arg2 "arg31 arg32"'
echo ""
$shell script/getarguments.sh arg1 arg2 "arg31 arg32"
echo "--------------------------------------------------------------------------"
echo "Test 2: Source the script"
echo source 'script/getarguments.sh arg1 arg2 "arg31 arg32"'
echo ""
source script/getarguments.sh arg1 arg2 "arg31 arg32"
echo "--------------------------------------------------------------------------"
echo "Test 3: Execute the link"
echo $shell 'script/getARGUMENTS.sh arg1 arg2 "arg31 arg32"'
echo ""
$shell script/getARGUMENTS.sh arg1 arg2 "arg31 arg32"
echo "--------------------------------------------------------------------------"
echo "Test 4: Source the link"
echo source 'script/getARGUMENTS.sh arg1 arg2 "arg31 arg32"'
echo ""
source script/getARGUMENTS.sh arg1 arg2 "arg31 arg32"
echo "--------------------------------------------------------------------------"
