#!/bin/bash

echo "Print variables from child:"
echo ""

if [ -z "$PARENT_LOCAL_VAR1" ]; then
    echo '$PARENT_LOCAL_VAR1'" doesn't exist"
else
    echo '$PARENT_LOCAL_VAR1'" = $PARENT_LOCAL_VAR1"
fi

if [ -z "$PARENT_LOCAL_VAR2" ]; then
    echo '$PARENT_LOCAL_VAR2'" doesn't exist"
else
    echo '$PARENT_LOCAL_VAR2'" = $PARENT_LOCAL_VAR2"
fi

if [ ${#PARENT_LOCAL_ARR1[@]} -eq 0 ]; then
    echo '$PARENT_LOCAL_ARR1'" doesn't exist"
else
    echo '$PARENT_LOCAL_ARR1'" = $PARENT_LOCAL_ARR1"
fi


if [ -z "$PARENT_ENVIRONMENT_VAR1" ]; then
    echo '$PARENT_ENVIRONMENT_VAR1'" doesn't exist"
else
    echo '$PARENT_ENVIRONMENT_VAR1'" = $PARENT_ENVIRONMENT_VAR1"
fi

if [ -z "$PARENT_ENVIRONMENT_VAR2" ]; then
    echo '$PARENT_ENVIRONMENT_VAR2'" doesn't exist"
else
    echo '$PARENT_ENVIRONMENT_VAR2'" = $PARENT_ENVIRONMENT_VAR2"
fi

if [ ${#PARENT_ENVIRONMENT_ARR1[@]} -eq 0 ]; then
    echo '$PARENT_ENVIRONMENT_ARR1'" doesn't exist"
else
    echo '${PARENT_ENVIRONMENT_ARR1[@]}'" = ${PARENT_ENVIRONMENT_ARR1[@]}"
fi


if [ -z "$CHILD_LOCAL_VAR1" ]; then
    echo '$CHILD_LOCAL_VAR1'" doesn't exist"
else
    echo '$CHILD_LOCAL_VAR1'" = $CHILD_LOCAL_VAR1"
fi

if [ -z "$CHILD_LOCAL_VAR2" ]; then
    echo '$CHILD_LOCAL_VAR2'" doesn't exist"
else
    echo '$CHILD_LOCAL_VAR2'" = $CHILD_LOCAL_VAR2"
fi

if [ ${#CHILD_LOCAL_ARR1[@]} -eq 0 ]; then
    echo '$CHILD_LOCAL_ARR1'" doesn't exist"
else
    echo '${CHILD_LOCAL_ARR1[@]}'" = ${CHILD_LOCAL_ARR1[@]}"
fi
