# Introduction

Discussion about parsing options to a shell script by using environment
variables.

# Quick answer

Prefere environment variables over locals and use this mechanism for:
- bunch of scripts interacting together and sharing/using same informations
- in an automated system with a centralized instance which controls the
  settings of the environment

Don't use this mechanism if a user should run this script and shall be able to
parse options into the script. In that case use shell arguments or `getopts`.

# Description

In a shell process there can be two different types of variables. There can be
local and environment variables. If an interactive shell starts a new shell or
executing a script, a new child process is spawned. This process inherits the
environment of the parent process, not the local stuff.

If you source a shell script inside an interactive shell no new process is
spawned. Therefore, the shell script can access the local stuff of the parent.

When starting a new shell or executing a script it's possible to create a local
variable for the child directly at the invocation.

So, we have three different possibilities to parse information to the script,
using variables. And we have to taken into account how the script is invoked:
```
LOC_VAR=a
export ENV_VAR=a

bash script.sh         # <-- has access to ENV_VAR only
source script.sh       # <-- has access to ENV_VAR and LOC_VAR
VAR=a bash script.sh   # <-- has access to ENV_VAR and VAR
VAR=a source script.sh # <-- has access to ENV_VAR and LOC_VAR and VAR
```

You can run `test.sh` which sets some different variables and invokes
`script/getenvironment.sh`. This script tries to access the variables and
prints the result. There is also a symbolic link to it
`script/getENVIRONMENT.sh`, but the name of the link differs from the script
name.

Per default the `bash` is used to execute the example script. With the first
argument you can pass another shell like `test.sh sh`.

# Conclusion

Environment variables can always be parsed into a shell script. All child process
can access them.

Using local variables is also possbile if you split a huge script into smaller
parts and source them into a 'master' script.

The shell itself and many other utilities are already using environment
variables, like `PATH`, `HOME` and `PWD`.

It's useful if many scripts need some shared informations. But each child
process can change the values. Handling the environment variables as read only
is a good practice. Setting and changing should be done from a centralized
place, similiar to `.bashrc` or `/etc/basrc`. Keep in mind there is only one
global namespace.

Environment variables should not be used for a single script to parse options
by the user. You have to deal with missing but expected variables, wrong values
and namespace pollution. You have no common call interface for your script.
For this purpose use shell arguments or builtin command `getopts`.

# References

- [Opengroup - Environment Variables](https://pubs.opengroup.org/onlinepubs/9699919799/basedefs/V1_chap08.html#tag_08)
- [GNU Bash - Environment](https://www.gnu.org/software/bash/manual/bash.html#Environment)
- [GNU Bash - Shell Parameters](https://www.gnu.org/software/bash/manual/bash.html#Shell-Parameters)
