#!/bin/bash

# default shell
shell=bash

# the first argument of the script is treated as the shell to use
[[ ! -z $1 ]] && shell=$1

#
# create some local variables
#
PARENT_LOCAL_VAR1=a
PARENT_LOCAL_VAR2="b c"
PARENT_LOCAL_ARR1=(d e "f g")

#
# create some environment variables
#
export PARENT_ENVIRONMENT_VAR1=a
export PARENT_ENVIRONMENT_VAR2="b c"
export PARENT_ENVIRONMENT_ARR1=(d e "f g")

#
# execute different test cases
#
echo "--------------------------------------------------------------------------"
echo "Test 1: Execute the script"
echo ""
echo '$ CHILD_LOCAL_VAR1=a \'
echo '    CHILD_LOCAL_VAR2="b c" \'
echo '    CHILD_LOCAL_ARR1=(d e "f g") \'
echo "    $shell script/getenvironment.sh"
echo ""
CHILD_LOCAL_VAR1=a \
    CHILD_LOCAL_VAR2="b c" \
    CHILD_LOCAL_ARR1=(d e "f g") \
    $shell script/getenvironment.sh
echo "--------------------------------------------------------------------------"
echo "Test 2: Source the script"
echo ""
echo '$ CHILD_LOCAL_VAR1=a \'
echo '    CHILD_LOCAL_VAR2="b c" \'
echo '    CHILD_LOCAL_ARR1=(d e "f g") \'
echo '    source script/getenvironment.sh'
echo ""
CHILD_LOCAL_VAR1=a \
    CHILD_LOCAL_VAR2="b c" \
    CHILD_LOCAL_ARR1=(d e "f g") \
    source script/getenvironment.sh
echo "--------------------------------------------------------------------------"
echo "Test 3: Execute the link"
echo ""
echo '$ CHILD_LOCAL_VAR1=a \'
echo '    CHILD_LOCAL_VAR2="b c" \'
echo '    CHILD_LOCAL_ARR1=(d e "f g") \'
echo "    $shell script/getENVIRONMENT.sh"
echo ""
CHILD_LOCAL_VAR1=a \
    CHILD_LOCAL_VAR2="b c" \
    CHILD_LOCAL_ARR1=(d e "f g") \
    $shell script/getENVIRONMENT.sh
echo "--------------------------------------------------------------------------"
echo "Test 4: Source the link"
echo ""
echo '$ CHILD_LOCAL_VAR1=a \'
echo '    CHILD_LOCAL_VAR2="b c" \'
echo '    CHILD_LOCAL_ARR1=(d e "f g") \'
echo '    source script/getENVIRONMENT.sh'
echo ""
CHILD_LOCAL_VAR1=a \
    CHILD_LOCAL_VAR2="b c" \
    CHILD_LOCAL_ARR1=(d e "f g") \
    source script/getENVIRONMENT.sh
echo "--------------------------------------------------------------------------"

