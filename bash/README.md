# Introduction

This is a collection of different `bash` topics. I try to find answers and best
or common practises for frequently asked questions. Currently there is no
specific or common structure covering all topics. Each problem or each question
is discussed in a separate subdirectory and it's treated as a sub project.
Check the correspondig reamde file to get further information about the
question itself, the structure of the sub project and the answer and
conclusion:

- [Get the name of a running shell script inside the script
  itself](script-name/README.md)
- [Get the path to a running shell script inside the script
  itself](script-path/README.md)
- [Determine inside a script if the script is sourced or
  not](am-i-sourced/README.md)
- [How to exit/return a script](exit-strategy/README.md)
- [How to add options to your script](script-options/README.md)
