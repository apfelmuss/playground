# Introduction

Try to find a way how to get the path of a shell script inside the script
itself.

# Quick answer

For Bash, no POSIX-compliant:
```
scriptPath="$( dirname -- "$( readlink -f -- "${BASH_SOURCE[0]}" )" )"
```

# Description

A POSIX-shell like `sh` has an environment variable `$0` which holds the name
of the script with the relative path to it from location of invocation.

The GNU Bash has an array variable `$BASH_SOURCE` where the first entry holds
the name of the script with the relative path to it from the location of
invocation.

There a different possibilities how the script was invoked:
- `bash <SCRIPT>`
- `source <SCRIPT>`
- Through a symbolic link from another location.

# Test

There is an example script `script/getpath.sh` where different approaches are
used to get the name of the script file. There is also a symbolic link to it
`script/subdir/getPATH.sh` stored in another location.

Just run `test.sh` to see the different results. Per default the `bash` is used
to execute the example script. With the first argument you can pass another
shell like `test.sh sh`.

# Conclusion

The variables `$0` and `${BASH_SOURCE[0]}` returning the name of the script
with the relative path to it, from the location where the script was invoked.

With `dirname` you can remove the file name and get the path to the file. It's
the string till last `/`. But this is only the path difference between current
working path (the location from which the script is invoked) and the location
where the script is stored. Therefore, we can change into thät directory (`cd`)
temporary in a sub shell and determine the absolute path of that location with
`pwd`.

This works if the script was invoked by `./<path>/<script>` or `bash
<path>/<script>`

But if the script is invoked via `source`, `$0` returns always the name of the
source itself (parent process), no matter what is sourced.

--> To get always the relative path to the script, regardless how it is
invoked, use: `${BASH_SOURCE[0]}`. Keep in mind, this works only in the bash
and not in a POSIX compliant shell like `sh`.

But a symbolic link isn't resolved. You get the path to the link, not the file
path to which the link points to.

--> Use `readlink -f` to resovle the link. We are getting already the absolute
path to the script. This is also not a POSIX utility.

**So if you are inside a bash script and you don't need to be POSIX compliant
use:**
```
scriptPath="$( dirname -- "$( readlink -f -- "${BASH_SOURCE[0]}" )" )"
```

# References

- [sh](https://pubs.opengroup.org/onlinepubs/9699919799/utilities/sh.html)
- [sh command language - Special parameters](https://pubs.opengroup.org/onlinepubs/9699919799/utilities/V3_chap02.html#tag_18_05_02)
- [GNU Bash - Special parameters](https://www.gnu.org/software/bash/manual/bash.html#Special-Parameters)
- [GNU Bash - Bash variables](https://www.gnu.org/software/bash/manual/bash.html#Bash-Variables)
- [Open Group - dirname](https://pubs.opengroup.org/onlinepubs/9699919799/functions/dirname.html#tag_16_91)
- [GNU Coreutils - dirname](https://www.gnu.org/software/coreutils/manual/html_node/dirname-invocation.html)
- [GNU Coreutils - readlink](https://www.gnu.org/software/coreutils/manual/html_node/readlink-invocation.html)
- [Stackoverflow](https://stackoverflow.com/a/29835459)
