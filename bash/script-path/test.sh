#!/bin/bash

# default shell
shell=bash

# the first argument of the script is treated as the shell to use
[[ ! -z $1 ]] && shell=$1

echo "--------------------------------------------------------------------------"
echo "Test 1: Execute the script"
echo "'\$ $shell script/getpath.sh'"
echo ""
$shell script/getpath.sh
echo "--------------------------------------------------------------------------"
echo "Test 2: Source the script"
echo "'\$ source script/getpath.sh'"
echo ""
source script/getpath.sh
echo "--------------------------------------------------------------------------"
echo "Test 3: Execute the symbolic link"
echo "'\$ $shell script/subdir/getPATH.sh'"
echo ""
$shell script/subdir/getPATH.sh
echo "--------------------------------------------------------------------------"
echo "Test 4: Source the symbolic link"
echo "'\$ source script/subdir/getPATH.sh'"
echo ""
source script/subdir/getPATH.sh
echo "--------------------------------------------------------------------------"
