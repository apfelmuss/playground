#!/bin/bash

sol1="$( dirname -- "$0" )"
sol2="$( dirname -- "${BASH_SOURCE[0]}" )"
sol3="$( cd -- "$( dirname -- "$0" )" && pwd -P )"
sol4="$( cd -- "$( dirname -- "${BASH_SOURCE[0]}" )" && pwd -P )"
sol5="$( dirname -- "$( readlink -f -- "$0" )" )"
sol6="$( dirname -- "$( readlink -f -- "${BASH_SOURCE[0]}" )" )"

echo '"$0"'"                                                        = $0"
echo '"${BASH_SOURCE[0]}"'"                                         = ${BASH_SOURCE[0]}"
echo '"$( dirname -- "$0" )"'"                                      = $sol1"
echo '"$( dirname -- "${BASH_SOURCE[0]}" )"'"                       = $sol2"
echo '"$( cd -- "$( dirname -- "$0" )" && pwd -P )"'"               = $sol3"
echo '"$( cd -- "$( dirname -- "${BASH_SOURCE[0]}" )" && pwd -P )"'"= $sol4"
echo '"$( dirname -- "$( readlink -f -- "$0" )" )"'"                = $sol5"
echo '"$( dirname -- "$( readlink -f -- "${BASH_SOURCE[0]}" )" )"'" = $sol6"
