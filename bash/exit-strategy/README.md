# Introduction

Try to find a way how to cancel a running script if an error occurs.

# Quick answer

For Bash, no POSIX-compliant:
```
[[ "$0" == "${BASH_SOURCE[0]}" ]] && exitCommand=exit || exitCommand=return
# on error use '$exitCommand 1'
# last line of script '$exitCommand 0'
```

# Description

In a shell script you can use the command `exit` to stop the execution
immediately. This causes in killing the script process. If you execute the
script from an interactive shell or from another script a new shell process
will be spawned as a child process. The command `exit` terminates this child
process. The cally can examine `$?` to get return code and see if the script
has finished normally or was terminated by an error.

But if the script is sourced no new process is started. The script is running
inside the parent process. Therefore, this process is killed by `exit`. For
that case you can use the command `return`. The script stops immediately,
returns the exit code but do not terminate the process. Then the cally is still
able to examine `$?` to be able to distinguish between a normal or faulty
script execution.

So, what to do if you want to create a script which is able to be normally
executed and sourced? Or you don't know how the user calls the script?

# Test

There is an example script `script/exitStrategy.sh` where different approaches
are used to cancel the script execution. There is also a symbolic link to it
`script/cancelStrategy.sh`, but the name of the link differs from the script
name.

Just run `test.sh` to see the different results. Per default the `bash` is used
to execute the example script. With the first argument you can pass another
shell like `test.sh sh`.

# Conclusion

The variables `$0` and `${BASH_SOURCE[0]}` returning the name of the script
with the relative path to it, from the location where the script was invoked.

But if the script is invoked via `source`, `$0` returns always the name of the
source itself (parent process), no matter what is sourced.

To get always the script name (with relative path to it), regardless how it is
invoked, use: `${BASH_SOURCE[0]}`. Keep in mind, this works only in the bash
and not in a POSIX compliant shell like `sh`.

Therefore, we can just compare this two variables to figure out how to
terminate the script.

**So if you are inside a bash script and you don't need to be POSIX compliant
use:**

```
[[ "$0" == "${BASH_SOURCE[0]}" ]] && exitCommand=exit || exitCommand=return

[ ! $error = 0 ] && $exitCommand 1

$exitCommand 0
```

Hint: If you want to always source a script, don't add the suffix `.sh`
and don't add the execution flags to that file.

# References

- [sh](https://pubs.opengroup.org/onlinepubs/9699919799/utilities/sh.html)
- [sh command language - exit](https://pubs.opengroup.org/onlinepubs/9699919799/utilities/V3_chap02.html#tag_18_21)
- [sh command language - return](https://pubs.opengroup.org/onlinepubs/9699919799/utilities/V3_chap02.html#tag_18_24)
- [GNU Bash](https://www.gnu.org/software/bash/manual/bash.html)
- [GNU Bash - Builtin Commands](https://.gnu.org/software/bash/manual/bash.html#Special-Parameter://www.gnu.org/software/bash/manual/bash.html#Shell-Builtin-Commands)
