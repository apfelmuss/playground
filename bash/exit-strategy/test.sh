#!/bin/bash

# default shell
shell=bash

# the first argument of the script is treated as the shell to use
[[ ! -z $1 ]] && shell=$1

echo "--------------------------------------------------------------------------"
echo "Test 1: Execute the script"
echo "'\$ $shell script/exitStrategy.sh"
echo ""
$shell script/exitStrategy.sh
echo "cancelled with code $?"
echo "--------------------------------------------------------------------------"
echo "Test 2: Source the script"
echo "'\$ source script/exitStrategy.sh"
echo ""
source script/exitStrategy.sh
echo "cancelled with code $?"
echo "--------------------------------------------------------------------------"
echo "Test 3: Execute the link"
echo "'\$ $shell script/cancelStrategy.sh"
echo ""
$shell script/cancelStrategy.sh
echo "cancelled with code $?"
echo "--------------------------------------------------------------------------"
echo "Test 4: Source the link"
echo "'\$ source script/cancelStrategy.sh"
echo ""
source script/cancelStrategy.sh
echo "cancelled with code $?"
echo "--------------------------------------------------------------------------"
