#!/bin/bash

[[ "$0" == "${BASH_SOURCE[0]}" ]] && exitCommand=exit || exitCommand=return

echo "cancel script with command '$exitCommand'"

$exitCommand 1
